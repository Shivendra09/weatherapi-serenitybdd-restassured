# WeatherStack API RestAssuredSerenityCucumber Framework

This is a sample Rest API test sample endpoints is one of the API from https://any-api.com/ available with API here https://weatherstack.com/documentation and with base api http://api.weatherstack.com/ The published public APIs represent a several weather forcast options:

There are several API endpoints to choose from:
========All API endpoints of WeatherStack use GET method only ===============
1. Current Weather: **GET** current weather data. (Available with **Free Plan** with **limited number of hits** using **_ACCESS KEY_**)
2. Historical Weather: **GET** historical weather data. **PAID** (Available with monthly subscription plan)
3. Historical Time-Series: **GET** historical time-series weather data. **PAID** (Available with monthly subscription plan)
4. Weather Forecast: **GET** weather forecast for up to 14 days. **PAID** (Available with monthly subscription plan)
5. Location Lookup: **GET** Look up one or multiple locations. **PAID** (Available with monthly subscription plan)

**Base URL:** API requests start out with the following base URL:

http://api.weatherstack.com/

Tests are written using a combination of SerenityBDD, RestAssured, Cucumber, Junit & Maven with bit of Spring uses.

## Technology Stack

- Java
- Serenity BDD
- Maven
- RestAssured
- Cucumber

## Prerequisites

* [Java 1.8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - Java Dev Kit at least use 1.8 however, in this project use jdk 11
* [Maven](https://maven.apache.org/download.cgi) - Dependency Manager

## Application Under Test

Used below Restful-WeatherStack Free version of API endpoint as the Application Under Test.

* URL : http://api.weatherstack.com/current
* 

## The project directory structure
The project follows the standard directory structure used in most Serenity projects:

```Gherkin

schema
  +weatherStack
    +schema                            schema validation of api json response
src
  + main
    + java
      +com.weatherstack.common
         + env                         methods to get/set env related configs
         + model                       pojos of endpoint
         + utilits                    utility methods and constants
  + test
    + java
      + com.weatherstack.api
         + config                      specifying each Step class as @SpringBootTest and its @ContextConfiguration,modules to scan, properties to use etc
         + endpoints                   endpoints of the services
         + runners                     test runner(senerity runner/trigger configurations)
         + steps                       Step definitions for the BDD feature
         + utils                       Common utility methods
    + resources
      + features                       Feature files
      + properties                     AUT properties files
      + testdata                        an example to use csv file for data driven test using /**@RunWith(SerenityParameterizedRunner.class)**

```
Following instructions will help you running the project. 

### Download and unzipped project on local

### Installation and Test Execution

Open the project in any IDE Eclipse/IntelliJ. Run the following command in Terminal and build the project. It will automatically download all the required dependencies.

```sh
$ mvn clean install
```

### Execute Tests

Run the following command in Terminal to execute tests.

```sh
$ mvn clean verify
```

### Test Report

You can find the Serenity reports in the following directory of the Project.

```sh
\target\site\serenity\
```

In the serenity directory, open 'index.html' file to view the report.

Please note from implementation perspective of this assignment against single GET request:
## Cucumber feature file:
I have used several ways to implement the scenarios like, with Examples as Scenario OutLine, with Datatable as Scenario, simple scenario, Background, and some other where I am generating faker lib values to avoid duplicates. 
However, there are still more ways and more use cases we can build using cucumber in combination with rest assured.
Moreover, In order to write test case just **add new scenario in .feature file>create stepDef in stepDef class> create logic and method with required input in endpoints folder> use .common package to add or use new helper**.
## Serenity BDD:
Here I have used the step defs to put little bit of logic however, we can make seperate Step class and can be used by using @Steps annotation
Here above, I have also mentioned that, we can also perform Data driven test without writing cucumber file. I am attaching screenshot  and sample class unused in 'com.weatherstack.api/utils/TestData' as a sample in project.
## Running test Parallel 
Here, I am running test parallelly with all scenarios independent to each other, please refer the pom.xml for more details
## For the logs pLease refer the terminalLog.txt and gitLabBuild&TestLog.txt pipeline line using simple gitlab-ci.yml file
In gitlab yml file we can also add after script something like below as well:
```
after_script:
- rm -rf /var/lib/apt/lists/*
- apt-get update
- apt-cache gencaches
- apt-get install -y zip unzip
- zip -r serenity_report.zip ./target/site/serenity
artifacts:
when: always
expire_in: 1 week
paths:
- ./serenity_report.zip
```




