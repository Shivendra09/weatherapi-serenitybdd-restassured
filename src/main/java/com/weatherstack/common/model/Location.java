package com.weatherstack.common.model;


import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/** @author shivendrasingh */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "country",
        "region",
        "lat",
        "lon",
        "timezone_id",
        "localtime",
        "localtime_epoch",
        "utc_offset"
})
public class Location {
    @JsonProperty("name")
    private String name;
    @JsonProperty("country")
    private String country;
    @JsonProperty("region")
    private String region;
    @JsonProperty("lat")
    private String lat;
    @JsonProperty("lon")
    private String lon;
    @JsonProperty("timezone_id")
    private String timezone_id;
    @JsonProperty("localtime")
    private String localtime;
    @JsonProperty("localtime_epoch")
    private int localtime_epoch;
    @JsonProperty("utc_offset")
    private String utc_offset;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    @JsonProperty("region")
    public String getRegion() {
        return region;
    }

    @JsonProperty("region")
    public void setRegion(String region) {
        this.region = region;
    }

    @JsonProperty("lat")
    public String getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(String lat) {
        this.lat = lat;
    }

    @JsonProperty("lon")
    public String getLon() {
        return lon;
    }

    @JsonProperty("lon")
    public void setLon(String lon) {
        this.lon = lon;
    }

    @JsonProperty("timezone_id")
    public String getTimezone_id() {
        return timezone_id;
    }

    @JsonProperty("timezone_id")
    public void setTimezone_id(String timezone_id) {
        this.timezone_id = timezone_id;
    }

    @JsonProperty("localtime")
    public String getLocaltime() {
        return localtime;
    }

    @JsonProperty("localtime")
    public void setLocaltime(String localtime) {
        this.localtime = localtime;
    }

    @JsonProperty("localtime_epoch")
    public int getLocaltime_epoch() {
        return localtime_epoch;
    }

    @JsonProperty("localtime_epoch")
    public void setLocaltime_epoch(int localtime_epoch) {
        this.localtime_epoch = localtime_epoch;
    }

    @JsonProperty("utc_offset")
    public String getUtc_offset() {
        return utc_offset;
    }

    @JsonProperty("utc_offset")
    public void setUtc_offset(String utc_offset) {
        this.utc_offset = utc_offset;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Location location = (Location) o;
        return localtime_epoch == location.localtime_epoch && name.equals(location.name) && country.equals(location.country) && region.equals(location.region) && lat.equals(location.lat) && lon.equals(location.lon) && timezone_id.equals(location.timezone_id) && localtime.equals(location.localtime) && utc_offset.equals(location.utc_offset) && additionalProperties.equals(location.additionalProperties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, country, region, lat, lon, timezone_id, localtime, localtime_epoch, utc_offset, additionalProperties);
    }
}