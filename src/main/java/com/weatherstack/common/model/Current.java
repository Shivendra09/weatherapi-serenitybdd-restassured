package com.weatherstack.common.model;


import com.fasterxml.jackson.annotation.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author shivendrasingh
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "observation_time",
        "temperature",
        "weather_code",
        "weather_icons",
        "weather_descriptions",
        "wind_speed",
        "wind_degree",
        "wind_dir",
        "pressure",
        "precip",
        "humidity",
        "cloudcover",
        "feelslike",
        "uv_index",
        "visibility",
        "is_day"
})
public class Current {
    @JsonProperty("observation_time")
    private String observation_time;
    @JsonProperty("temperature")
    private Integer temperature;
    @JsonProperty("weather_code")
    private Integer weather_code;
    @JsonProperty("weather_icons")
    private String[] weather_icons;
    @JsonProperty("weather_descriptions")
    private String[] weather_descriptions;
    @JsonProperty("wind_speed")
    private Integer wind_speed;
    @JsonProperty("wind_degree")
    private Integer wind_degree;
    @JsonProperty("wind_dir")
    private String wind_dir;
    @JsonProperty("pressure")
    private Integer pressure;
    @JsonProperty("precip")
    private Integer precip;
    @JsonProperty("humidity")
    private Integer humidity;
    @JsonProperty("cloudcover")
    private Integer cloudcover;
    @JsonProperty("feelslike")
    private Integer feelslike;
    @JsonProperty("uv_index")
    private Integer uv_index;
    @JsonProperty("visibility")
    private Integer visibility;
    @JsonProperty("is_day")
    private String is_day;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("observation_time")
    public String getObservation_time() {
        return observation_time;
    }

    @JsonProperty("observation_time")
    public void setObservation_time(String observation_time) {
        this.observation_time = observation_time;
    }

    @JsonProperty("temperature")
    public Integer getTemperature() {
        return temperature;
    }

    @JsonProperty("temperature")
    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    @JsonProperty("weather_code")
    public Integer getWeather_code() {
        return weather_code;
    }

    @JsonProperty("weather_code")
    public void setWeather_code(Integer weather_code) {
        this.weather_code = weather_code;
    }

    @JsonProperty("weather_icons")
    public String[] getWeather_icons() {
        return weather_icons;
    }

    @JsonProperty("weather_icons")
    public void setWeather_icons(String[] weather_icons) {
        this.weather_icons = weather_icons;
    }

    @JsonProperty("weather_descriptions")
    public String[] getWeather_descriptions() {
        return weather_descriptions;
    }

    @JsonProperty("weather_descriptions")
    public void setWeather_descriptions(String[] weather_descriptions) {
        this.weather_descriptions = weather_descriptions;
    }

    @JsonProperty("wind_speed")
    public Integer getWind_speed() {
        return wind_speed;
    }

    @JsonProperty("wind_speed")
    public void setWind_speed(Integer wind_speed) {
        this.wind_speed = wind_speed;
    }

    @JsonProperty("wind_degree")
    public Integer getWind_degree() {
        return wind_degree;
    }

    @JsonProperty("wind_degree")
    public void setWind_degree(Integer wind_degree) {
        this.wind_degree = wind_degree;
    }

    public String getWind_dir() {
        return wind_dir;
    }

    public void setWind_dir(String wind_dir) {
        this.wind_dir = wind_dir;
    }

    @JsonProperty("pressure")
    public Integer getPressure() {
        return pressure;
    }

    @JsonProperty("pressure")
    public void setPressure(Integer pressure) {
        this.pressure = pressure;
    }

    @JsonProperty("precip")
    public Integer getPrecip() {
        return precip;
    }

    @JsonProperty("precip")
    public void setPrecip(Integer precip) {
        this.precip = precip;
    }

    @JsonProperty("humidity")
    public Integer getHumidity() {
        return humidity;
    }

    @JsonProperty("humidity")
    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }

    @JsonProperty("cloudcover")
    public Integer getCloudcover() {
        return cloudcover;
    }

    @JsonProperty("cloudcover")
    public void setCloudcover(Integer cloudcover) {
        this.cloudcover = cloudcover;
    }

    @JsonProperty("feelslike")
    public Integer getFeelslike() {
        return feelslike;
    }

    @JsonProperty("feelslike")
    public void setFeelslike(Integer feelslike) {
        this.feelslike = feelslike;
    }

    @JsonProperty("uv_index")
    public Integer getUv_index() {
        return uv_index;
    }

    @JsonProperty("uv_index")
    public void setUv_index(Integer uv_index) {
        this.uv_index = uv_index;
    }

    @JsonProperty("visibility")
    public Integer getVisibility() {
        return visibility;
    }

    @JsonProperty("visibility")
    public void setVisibility(Integer visibility) {
        this.visibility = visibility;
    }

    @JsonProperty("is_day")
    public String getIs_day() {
        return is_day;
    }

    @JsonProperty("is_day")
    public void setIs_day(String is_day) {
        this.is_day = is_day;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Current current = (Current) o;
        return observation_time.equals(current.observation_time) && temperature.equals(current.temperature) && weather_code.equals(current.weather_code) && Arrays.equals(weather_icons, current.weather_icons) && Arrays.equals(weather_descriptions, current.weather_descriptions) && wind_speed.equals(current.wind_speed) && wind_degree.equals(current.wind_degree) && wind_dir.equals(current.wind_dir) && pressure.equals(current.pressure) && precip.equals(current.precip) && humidity.equals(current.humidity) && cloudcover.equals(current.cloudcover) && feelslike.equals(current.feelslike) && uv_index.equals(current.uv_index) && visibility.equals(current.visibility) && is_day.equals(current.is_day) && Objects.equals(additionalProperties, current.additionalProperties);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(observation_time, temperature, weather_code, wind_speed, wind_degree, wind_dir, pressure, precip, humidity, cloudcover, feelslike, uv_index, visibility, is_day, additionalProperties);
        result = 31 * result + Arrays.hashCode(weather_icons);
        result = 31 * result + Arrays.hashCode(weather_descriptions);
        return result;
    }
}
