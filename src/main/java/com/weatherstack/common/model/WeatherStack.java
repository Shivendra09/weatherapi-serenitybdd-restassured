package com.weatherstack.common.model;

import java.util.Objects;

/**
 * @author shivendrasingh
 */
public class WeatherStack
{
    private Request request;
    private Location location;
    private Current current;

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Current getCurrent() {
        return current;
    }

    public void setCurrent(Current current) {
        this.current = current;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        var weatherStack = (WeatherStack) o;
        return Objects.equals(request, weatherStack.request) && Objects.equals(location, weatherStack.location) && Objects.equals(current, weatherStack.current);
    }

    @Override
    public int hashCode() {
        return Objects.hash(request, location, current);
    }
}
