package com.weatherstack.common.env;


import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.weatherstack.common.utilits.FileOperations;
import com.weatherstack.common.utilits.FilePathBuilder;
import com.weatherstack.common.utilits.ApplicationConstants;

/**
 * 
 * This class is the implementation of application properties of AUT
 */

public class ApplicationProperties {

	private final Logger logger = LoggerFactory.getLogger(ApplicationProperties.class);

	private String weatherBaseApiUrl;

	private String weatherStackValidAccessKey;
	private String weatherStackInvalidAccessKey;
	private String weatherStackMissingAccessKey;
	private String weatherStackInvalidBaseUrl;

	private Map<String, String> additionalProps = null;

	protected ApplicationProperties() {
		additionalProps = new HashMap<>(5);
	}

	protected void loadProperties() {
		FilePathBuilder fpb = new FilePathBuilder(ApplicationConstants.ENVIORNMENT_PROPS);
		fpb.setParentDirectory(ApplicationConstants.PROPERTIES_DIRECTORY);

		String envProps = fpb.getFilePath();
		logger.debug("Environment Properties Path {}", envProps);

		FileOperations fileOps = new FileOperations(new File(envProps));
		Map<String, String> props = fileOps.getPropValuesInMap();

		if (props == null) {
			logger.error("Failed to read the properties for the application from resouce:" + envProps);
			return;
		}

		for (Entry<String, String> entry : props.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();

			if (ApplicationConstants.APPLICATION_BASE_URL.equals(key)) {
				setWeatherBaseApiUrl(value);
			} else if (ApplicationConstants.APPLICATION_VALID_ACCESS_KEY.equals(key)) {
				setWeatherStackValidAccessKey(value);
			} else if (ApplicationConstants.APPLICATION_INVALID_ACCESS_KEY.equals(key)) {
				setWeatherStackInvalidAccessKey(value);			}
			else if (ApplicationConstants.APPLICATION_MISSING_ACCESS_KEY.equals(key)) {
				setWeatherStackMissingAccessKey(value);
			}
			else if (ApplicationConstants.APPLICATION_INVALID_BASE_URL.equals(key)) {
				setWeatherStackInvalidBaseUrl(value);
			} else {
				setProperty(key, value);
			}
		}
	}

	public String getWeatherBaseApiUrl() {
		return weatherBaseApiUrl;
	}

	public void setWeatherBaseApiUrl(String weatherBaseApiUrl) {
		this.weatherBaseApiUrl = weatherBaseApiUrl;
	}

	public String getWeatherStackValidAccessKey() {
		return weatherStackValidAccessKey;
	}

	public void setWeatherStackValidAccessKey(String weatherStackValidAccessKey) {
		this.weatherStackValidAccessKey = weatherStackValidAccessKey;
	}

	public String getWeatherStackInvalidAccessKey() {
		return weatherStackInvalidAccessKey;
	}

	public void setWeatherStackInvalidAccessKey(String weatherStackInvalidAccessKey) {
		this.weatherStackInvalidAccessKey = weatherStackInvalidAccessKey;
	}

	public String getWeatherStackMissingAccessKey() {
		return weatherStackMissingAccessKey;
	}

	public void setWeatherStackMissingAccessKey(String weatherStackMissingAccessKey) {
		this.weatherStackMissingAccessKey = weatherStackMissingAccessKey;
	}

	public String getWeatherStackInvalidBaseUrl() {
		return weatherStackInvalidBaseUrl;
	}

	public void setWeatherStackInvalidBaseUrl(String weatherStackInvalidBaseUrl) {
		this.weatherStackInvalidBaseUrl = weatherStackInvalidBaseUrl;
	}

	private void setProperty(String key, String value) {
		additionalProps.put(key, value);
	}

	public String getPropertyValue(String key) {
		return additionalProps.get(key);
	}
}