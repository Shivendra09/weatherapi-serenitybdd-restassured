package com.weatherstack.common.utilits;

/**
 * 
 * This class is for all constants used across framework
 */

public class ApplicationConstants {

	public static final String ENVIORNMENT_PROPS = "applicationUnderTest.properties";
	public static final String PROPERTIES_DIRECTORY = "properties";

	public static final String APPLICATION_BASE_URL = "weatherStack.baseUrl.value";
	public static final String APPLICATION_VALID_ACCESS_KEY = "weatherStack.validAccessKey.value";
	public static final String APPLICATION_INVALID_ACCESS_KEY = "weatherStack.invalidAccessKey.value";
	public static final String APPLICATION_MISSING_ACCESS_KEY = "weatherStack.missingAccessKey.value";
	public static final String APPLICATION_INVALID_BASE_URL = "weatherStack.invalidBaseUrl.value";
	
}