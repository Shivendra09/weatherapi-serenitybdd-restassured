@WeatherStack
Feature: WeatherStack - Current API Endpoint Test

  Background: WeatherStack- Current API Endpoint Url
    Given  WeatherStack current endpoint is available with valid api access key

  Scenario Outline: Verify the user can retrieve current weather information using city name
    Given User request the weather for <cityName>
    When User verify that status code is 200
    Then User validate for schema for json response
      |jsonSchemaValue|
      |weatherStack/schemas/currentApiResponse|
  And I verify weatherStack request response as per WeatherStack model of <cityName>
    Examples:
      | cityName  |
      | New Delhi |

  Scenario: Verify the user can retrieve the weather of multiple location
    When User request the current weather for multiple New Delhi;Paris;Sydney;Amsterdam location
    Then The response has following response code
      | code |
      |  200 |
      |  200 |
      |  200 |

  Scenario: Verify Json key value pair properties under request object in json response
    When User request the current weather for multiple London;New York location
    Then User verify the json response value
      | request.type	| City|
      | request.language| en|
      | request.unit 	| m|

 Scenario: Verify Json key value pair properties under current object present in json response
   Given User request weather for any location
   When User verify that status code is 200
   Then User verify that all required current key value pairs are not null

  Scenario: Verify Json key value pair properties under location object present in json response
    Given User request weather for any location
    When User verify that status code is 200
    Then User verify that all required location key value pairs are not null

 Scenario: Verify the json response with invalid access key
   When  User request current api endpoint with invalid key
   Then User validate for schema for json response
     |jsonSchemaValue|
     |weatherStack/schemas/invalidAccessKey|
   And User verify the json response value
     | error.type|invalid_access_key|
     | error.info|You have not supplied a valid API Access Key. [Technical Support: support@apilayer.com]|

  Scenario: Verify the json response with invalid location
    When  User request current api endpoint with invalid location
    Then User validate for schema for json response
      |jsonSchemaValue|
      |weatherStack/schemas/invalidLocationResponse|
    And User verify the json response value
      | error.type|request_failed|
      | error.info|Your API request failed. Please try again or contact support.|

  Scenario: verify the json response with missing access key
    When  User request current api endpoint with missing access key
    Then User validate for schema for json response
      |jsonSchemaValue|
      |weatherStack/schemas/missingAccessKey|
    And User verify the json response value
      | error.type|missing_access_key|
      | error.info|You have not supplied an API Access Key. [Required format: access_key=YOUR_ACCESS_KEY]|

  Scenario: verify the json response with missing query parameter
    When User request current api endpoint with missing query
    Then User validate for schema for json response
      |jsonSchemaValue|
      |weatherStack/schemas/missingQueryParameter|
    And User verify the json response value
      | error.type|missing_query|
      | error.info|Please specify a valid location identifier using the query parameter.|






