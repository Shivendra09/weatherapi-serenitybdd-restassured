package com.weatherstack.api.steps;

import com.github.javafaker.Faker;
import com.weatherstack.api.config.WeatherAbstractTestDefinition;
import com.weatherstack.api.endpoints.BaseEndPoints;
import com.weatherstack.api.endpoints.WeatherStackCurrentApiEndpoint;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import com.weatherstack.common.model.WeatherStack;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.weatherstack.common.utilits.DataManager;

import java.util.*;

public class WeatherStackCurrentEndpointStepDef extends WeatherAbstractTestDefinition {

  private final Logger logger = LoggerFactory.getLogger(WeatherStackCurrentEndpointStepDef.class);
  @Autowired
  private DataManager dataManager;
  WeatherStackCurrentApiEndpoint weatherStackCurrentApiEndpoint = new WeatherStackCurrentApiEndpoint();
  World world = new World();
  WeatherStack weatherStack = new WeatherStack();
  List<Response> responseList = new ArrayList<>();
  Faker faker = new Faker();

    @When("^User request the weather for (.*)$")
  public void theUserHasRequestedTheWeatherForDublin(String cityName) {
    logger.info("The user makes an request for the weather in : " + cityName);
    world.setResponse(weatherStackCurrentApiEndpoint.getWeatherStackByLocation(null,cityName));
  }

  @Given("WeatherStack current endpoint is available with valid api access key")
  public void weatherStackCurrentEndpointIsAvailableWithValidApiAccessKey() {
     world.setResponse(weatherStackCurrentApiEndpoint.getWeatherStackByLocation(null,"Almere"));
  }
  @Then("User validate for schema for json response")
  public void userValidateForFollowingRequiredDetailsInJsonResponse(DataTable table) {
    List<Map<String, String>> rows = table.asMaps(String.class, String.class);
    for (Map<String, String> columns : rows) {
      weatherStackCurrentApiEndpoint.validateJsonSchemaValidator(columns.get("jsonSchemaValue"),world.getResponse());
    }

  }
  @And("^User verify that status code is (.*)$")
  public void userVerifyThatStatusCodeIs(int status) {
    logger.info("The user makes an request for the weather in : " + status);
    weatherStackCurrentApiEndpoint.verifyResponseStatusCode(world.getResponse(), status);
  }
  @Then("User verify the json response value")
  public void userVerifyTheJsonResponse(Map<String, String> responseFields) {
      weatherStackCurrentApiEndpoint.verifyNestedArrayValueResponseKeyValues(responseFields, world.getResponse());
  }
    @And("^I verify weatherStack request response as per WeatherStack model of (.*)$")
    public void iVerifyWeatherStackRequestResponseAsPerWeatherStackModel(String cityName){
       world.setWeatherStack(weatherStack);
        weatherStackCurrentApiEndpoint.verifyWeatherStackValuesAreAsExpected(world.getResponse(), world.getWeatherStack());
    }
    @When("^User request the current weather for multiple (.*) location$")
    public void userRequestTheCurrentWeatherForMultipleLocation(String multipleLocation) {
        String[] weatherLocations = multipleLocation.split(";");
        List<String> weatherLocationsValues = new ArrayList<>(Arrays.asList(weatherLocations));
        for (int i = 0; i < weatherLocationsValues.size(); i++) {
          responseList.add(weatherStackCurrentApiEndpoint.getWeatherStackByLocation(null,weatherLocationsValues.get(i)));
        }
    }
    @Then("The response has following response code")
    public void theResponseHasFollowingResponseCode(DataTable dt) {
        List<List<String>> rows = dt.asLists(String.class);
        for (int i = 1; i < dt.cells().size(); i++) {
            List<String> columns = rows.get(i);
            weatherStackCurrentApiEndpoint.verifyResponseStatusCode(responseList.get(i - 1), Integer.parseInt(columns.get(0)));
        }
    }
    @Then("User verify json response with pre-defined set of weather condition value against json response")
    public void userVerifyJsonResponseWithPreDefinedSetOfWeatherConditionValueAgainstJsonResponse(DataTable dt) {
        List<List<String>> rows = dt.asLists(String.class);
        for (int i = 1; i < dt.cells().size(); i++) {
            List<String> columns = rows.get(i);
            weatherStackCurrentApiEndpoint.verifyResponseKeyValues( "current.weather_descriptions[0]",columns.get(0),responseList.get(i - 1));
        }
    }
    @Then("User verify that all required current key value pairs are not null")
    public void userVerifyThatAllRequiredCurrentKeyValuePairsAreNotNull() {
        weatherStackCurrentApiEndpoint.verifyWeatherStackCurrentResponseBody(world.getResponse());
    }

    @Then("User verify that all required location key value pairs are not null")
    public void userVerifyThatAllRequiredLocationKeyValuePairsAreNotNull() {
        weatherStackCurrentApiEndpoint.verifyWeatherStackLocationResponseBody(world.getResponse());

    }

    @When("User request current api endpoint with invalid key")
    public void userRequestCurrentApiEndpointWithInvalidKey() {
        world.setResponse(weatherStackCurrentApiEndpoint.getWeatherStackByLocationWithInvalidAccessKey(null,"Amsterdam"));

    }

    @Given("User request weather for any location")
    public void userRequestWeatherForAnyLocation() {
         world.setResponse(weatherStackCurrentApiEndpoint.getWeatherStackByLocation(null,faker.address().country()));
    }

    @When("User request current api endpoint with invalid location")
    public void userRequestCurrentApiEndpointWithInvalidLocation() {
        world.setResponse(weatherStackCurrentApiEndpoint.getWeatherStackByLocation(null,"@@@@@@@"));

    }

    @When("User request current api endpoint with missing access key")
    public void userRequestCurrentApiEndpointWithMissingAccessKey() {
      world.setResponse(weatherStackCurrentApiEndpoint.getWeatherStackByLocationWithMissingAccessKey(null,"Amsterdam"));
    }

    @When("User request current api endpoint with missing query")
    public void userRequestCurrentApiEndpointWithMissingQuery() {
        world.setResponse(weatherStackCurrentApiEndpoint.getWeatherStackByLocation(null,""));

    }
}



