package com.weatherstack.api.steps;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import com.weatherstack.common.model.*;

public class World {
    private Response response;
    private RequestSpecification req;
    private WeatherStack weatherStack;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public RequestSpecification getReq() {
        return req;
    }

    public void setReq(RequestSpecification req) {
        this.req = req;
    }

    public WeatherStack getWeatherStack() {
        return weatherStack;
    }

    public void setWeatherStack(WeatherStack weatherStack) {
        this.weatherStack = weatherStack;
    }

}
