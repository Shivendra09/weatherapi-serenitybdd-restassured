package com.weatherstack.api.endpoints;

import com.weatherstack.api.steps.WeatherStackCurrentEndpointStepDef;
import com.weatherstack.api.utils.Constants;
import com.weatherstack.common.model.WeatherStack;
import com.weatherstack.common.utilits.DataManager;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WeatherStackCurrentApiEndpoint extends BaseEndPoints {
    private final Logger logger = LoggerFactory.getLogger(WeatherStackCurrentEndpointStepDef.class);
    private static final String CURRENT_ENDPOINT_PATH = "/current";
    @Autowired
    private DataManager dataManager;

    public String getPath() {
        return CURRENT_ENDPOINT_PATH;
    }
    public Response getWeatherStackByLocation(RequestSpecification rSpec, String location) {
        rSpec = getCommonSpec().params("access_key", getAccessKey()).params("query",location).basePath(getBasePath());

        return sendRequest(rSpec, Constants.RequestType.GET_REQUEST, null);

    }
    public Response getWeatherStackByLocationWithInvalidAccessKey(RequestSpecification rSpec, String location) {
        rSpec = getCommonSpec().params("access_key", getInvalidAccessKey()).params("query",location).basePath(getBasePath());

        return sendRequest(rSpec, Constants.RequestType.GET_REQUEST, null);

    }
    public Response getWeatherStackByLocationWithMissingAccessKey(RequestSpecification rSpec, String location) {
        rSpec = getCommonSpec().params("access_key", getMissingAccessKey()).params("query",location).basePath(getBasePath());

        return sendRequest(rSpec, Constants.RequestType.GET_REQUEST, null);

    }
    private String getBasePath() {
        return CURRENT_ENDPOINT_PATH;
    }

    public void verifyWeatherStackValuesAreAsExpected(Response response, WeatherStack expectedWeatherStackResponseBoby){
        WeatherStack weatherStack = response.body().as(WeatherStack.class);
        weatherStack.equals(expectedWeatherStackResponseBoby); }
    public void verifyWeatherStackCurrentResponseBody(Response response){
         response.then().log().all();
         WeatherStack weatherStack = response.as(WeatherStack.class);
        Assert.assertFalse(weatherStack.getLocation().getCountry().isEmpty());
        Assert.assertFalse(weatherStack.getCurrent().getObservation_time().isEmpty());
        Assert.assertFalse(weatherStack.getCurrent().getIs_day().isEmpty());
        Assert.assertFalse(weatherStack.getCurrent().getWind_dir().isEmpty());
    }
    public void verifyWeatherStackLocationResponseBody(Response response){
        response.then().log().all();
        WeatherStack weatherStack = response.as(WeatherStack.class);
        Assert.assertFalse(weatherStack.getLocation().getCountry().isEmpty());
        Assert.assertFalse(weatherStack.getLocation().getLocaltime().isEmpty());
        Assert.assertFalse(weatherStack.getLocation().getLon().isEmpty());
        Assert.assertFalse(weatherStack.getLocation().getLat().isEmpty());
        Assert.assertFalse(weatherStack.getLocation().getName().isEmpty());
        Assert.assertFalse(weatherStack.getLocation().getTimezone_id().isEmpty());
        Assert.assertFalse(weatherStack.getLocation().getUtc_offset().isEmpty());
        Assert.assertFalse(weatherStack.getLocation().getUtc_offset().isEmpty());
    }
}
