package com.weatherstack.api.endpoints;
import com.github.fge.jsonschema.SchemaVersion;
import com.github.fge.jsonschema.cfg.ValidationConfiguration;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.weatherstack.api.steps.WeatherStackCurrentEndpointStepDef;
import io.restassured.module.jsv.JsonSchemaValidator;
import com.weatherstack.common.model.WeatherStack;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.junit.Assert;
import com.weatherstack.api.utils.Constants;
import com.weatherstack.common.env.ApplicationProperties;
import com.weatherstack.common.env.Environment;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.weatherstack.common.utilits.DataManager;

import java.io.File;
import java.util.Map;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static io.restassured.module.jsv.JsonSchemaValidatorSettings.settings;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class BaseEndPoints {
    ApplicationProperties appProps = Environment.INSTANCE.getApplicationProperties();
    private final Logger logger = LoggerFactory.getLogger(WeatherStackCurrentEndpointStepDef.class);

    String cwd = System.getProperty("user.dir");
    /**
     *  common specification for request
     */
    public RequestSpecification getCommonSpec() {
        RequestSpecification rSpec = SerenityRest.given();
        rSpec.contentType(ContentType.JSON).baseUri(appProps.getWeatherBaseApiUrl());
        return rSpec;
    }
    /**
     *  getting valid access_key value
     */
    public String getAccessKey(){
        return appProps.getWeatherStackValidAccessKey();
    }
    /**
     *  getting invalid access_key value
     */
    public String getInvalidAccessKey(){
        return appProps.getWeatherStackInvalidAccessKey();
    }
    /**
     *  getting missing access_key value
     */
    public String getMissingAccessKey(){
        return appProps.getWeatherStackMissingAccessKey();
    }
    /**
     *  getting invalid baseUrl value
     */
    public String getInvalidBaseUrl(){
        return appProps.getWeatherStackInvalidBaseUrl();
    }
    /**
     *  getting valid baseUrl value
     */
    public String getValidBaseUrl(){
        return appProps.getWeatherBaseApiUrl();
    }
    /**
     * Convert POJO to JSON
     */
    protected JSONObject createJSONPayload(Object pojo) {
        return new JSONObject(pojo);
    }

    /**
     * Verify that the response code is the same as expected code by comparing the
     * provided expected code and the response code from the response received by
     * sending the request
     */
    public void verifyResponseStatusCode(Response response, int expectedCode) {

        Assert.assertEquals(response.getStatusCode(), expectedCode);
    }
    /**
     * Verify response key value pair of json response using jsonpath
     */
    public void verifyResponseKeyValues(String key, String val, Response response) {
        String keyValue;
        keyValue = response.jsonPath().getString(key);
        assertThat(keyValue, is(val));
    }
    public String verifyResponseValues(String key, Response response) {
        String keyValue;
        keyValue = response.jsonPath().getString(key);
       // logger.info("Key value of weather code:"+keyValue);
       return keyValue;
    }
    /**
     * Verify response key value pair of json response for nested key value pair in json response
     */
    public void verifyNestedArrayValueResponseKeyValues(Map<String, String> responseFields, Response response) {
        for (Map.Entry<String, String> field : responseFields.entrySet()) {
            if (StringUtils.isNumeric(field.getValue())) {
                response.then().body(field.getKey(), equalTo(Integer.parseInt(field.getValue())));
                response.then().log().all();
            } else {
                response.then().body(field.getKey(), equalTo(field.getValue()));
            }
        }
    }

    public void verifyTrue(boolean val) {
        assertTrue(val);
    }

    public void verifyFalse(boolean val) {
        assertFalse(val);
    }
    /**
     * Verify json response schema
     */
    public void validateJsonSchemaValidator(String schemaFile, Response response) {
        JsonSchemaFactory factory = JsonSchemaFactory.newBuilder()
                .setValidationConfiguration(
                        ValidationConfiguration.newBuilder().setDefaultVersion(SchemaVersion.DRAFTV4).freeze())
                .freeze();
        JsonSchemaValidator.settings = settings().with().jsonSchemaFactory(factory).and().with()
                .checkedValidation(true);
        String path = cwd + "/schema/" + schemaFile + ".json";
        File f = new File(path);
        response.then().assertThat().body(matchesJsonSchema(f));
        response.then().log().all();
    }

    /**
     * Send request
     *
     * @param request     details for sending the request
     * @param requestType of the request. i.e GET, POST, PUT, DELETE, UPDATE
     * @param pojo        if provided will be added to the body of request as JSON
     *                    payload
     * @return response received from the service by sending the request
     */
    @Step
    public Response sendRequest(RequestSpecification request, int requestType, Object pojo) {
        Response response;

        // Add the Json to the body of the request
        if (pojo != null) {
            String payload = createJSONPayload(pojo).toString();
            request.body(payload);
        }

        // need to add a switch based on request type
        switch (requestType) {
            case Constants.RequestType.POST_REQUEST:
                if (request == null) {
                    response = SerenityRest.when().post();
                } else {
                    response = request.post();
                }
                break;
            case Constants.RequestType.DELETE_REQUEST:
                if (request == null) {
                    response = SerenityRest.when().delete();
                } else {
                    response = request.delete();
                }
                break;
            case Constants.RequestType.PUT_REQUEST:
                if (request == null) {
                    response = SerenityRest.when().put();
                } else {
                    response = request.put();
                }
                break;
            case Constants.RequestType.GET_REQUEST:
            default:
                if (request == null) {
                    response = SerenityRest.when().get();
                } else {
                    response = request.get();
                }
                break;
        }
        return response;
    }

}
