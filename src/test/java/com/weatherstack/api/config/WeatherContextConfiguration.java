package com.weatherstack.api.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;

@EnableRetry
@Configuration
@ComponentScan(basePackages = {"com.weatherstack.api", "com.weatherstack.common"})
public class WeatherContextConfiguration {
}
