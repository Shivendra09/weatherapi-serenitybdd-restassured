package com.weatherstack.api.config;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = {WeatherContextConfiguration.class})
public class WeatherAbstractTestDefinition {

}